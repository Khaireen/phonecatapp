(function (angular) {
    function checkmark() {
        return function (input) {
            return input ? '\u2713' : '\u2718'
        }
    }

    return angular
        .module('core')
        .filter('checkmark', checkmark);
})(angular);