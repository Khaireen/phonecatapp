(function (angular) {

    const EXTERNAL_DEPS = [
        'ngRoute',
        'ngAnimate'
    ];

    const OWN_DEPS = [
        'PhoneList',
        'PhoneDetail',
        'core'
    ];

    const DEPS = [...EXTERNAL_DEPS, ...OWN_DEPS]

    return angular
        .module('PhoneCatApp', DEPS);

})(angular);