(function (angular) {
    class PhoneListController {
        constructor(Phone) {
            var vm = this;
            vm.phones = Phone.query();
            vm.orderProp = 'age';
        }
    }

    return angular
        .module('PhoneList')
        .component('phoneList', {
            templateUrl: 'phone-list/phone-list.template.html',
            controller: PhoneListController
        })
})(angular)