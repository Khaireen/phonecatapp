(function (angular) {
    class controller {
        constructor(Phone, $routeParams) {
            var vm = this;

            vm.phone = Phone.get({ phoneId: $routeParams.phoneId },
                                   phone => vm.setImage(phone.images[0])
            );

            vm.setImage = function setImage(imageUrl) {
                vm.mainImageUrl = imageUrl;
            }
        }
    }

    return angular
        .module('PhoneDetail')
        .component('phoneDetail', {
            templateUrl: 'phone-detail/phone-detail.template.html',
            controller

        });
})(angular)