(function (angular) {

    return angular
        .module('PhoneDetail', ['ngRoute', 'core.phone']);
})(angular);