Remarks after code review:

- folder tree to flat
- should not use vm to bind this in ES6 syntax (use arrow functions instead)
- be careful with filters in views, they can radically reduce efficiency
- in phone-detail.component, line 10 - this should be own method
- $http recommended instead of $resource
- SEMICOLONS!!! :)