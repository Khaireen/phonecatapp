(function (angular) {

    class RoutesConfig {
        constructor($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');

            $routeProvider.
                when('/phones', {
                    template: '<phone-list></phone-list>'
                }).
                when('/phones/:phoneId', {
                    template: '<phone-detail></phone-detail>'
                }).
                otherwise('/phones')
        }
    }

    return angular
        .module('PhoneCatApp')
        .config(RoutesConfig);

})(angular)