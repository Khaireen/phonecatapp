describe('checkmark', function () {
    beforeEach(module('core'));

    it('should convert bollean values to unicode checkamrk or cross',
        inject(function (checkmarkFilter) {
            expect(checkmarkFilter(true)).toBe('\u2713');
            expect(checkmarkFilter(false)).toBe('\u2718');
        }));
});