describe('phoneList', function () {
    // Load the module containing the 'PhoneList' before each test
    beforeEach(module('PhoneList'));

    describe('controller', function () {
        var $httpBackend, ctrl;

        beforeEach(inject(function ($componentController, _$httpBackend_) {
            $httpBackend = _$httpBackend_;
            $httpBackend.expectGET('../phones/phones.json')
                        .respond([{ name: 'Nexus S' }, { name: 'Motorola DROID' }]);

            ctrl = $componentController('phoneList');
        }));
    })

    //  Test the controller
    describe('PhoneListComponent', function () {
        var ctrl;

        beforeEach(inject(function ($componentController) {
            ctrl = $componentController('phoneList');
        }));

        it('should set a default value for the `orderProp` model', function () {
            expect(ctrl.orderProp).toBe('age')
        })
    });


});