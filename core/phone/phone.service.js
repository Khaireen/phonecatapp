(function (angular) {
    function Phone($resource) {
        return $resource('phones/:phoneId.json', {}, {
            query: {
                method: 'GET',
                params: {phoneId: 'phones'},
                isArray: true
            }
        });
    }

    return angular
        .module('core.phone')
        .factory('Phone', Phone);
})(angular);